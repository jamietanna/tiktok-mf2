@app
tiktok-mf2

@http
get /
get /video
get /logs

@tables
logs
  id *String
  expires TTL

@tables-indexes
logs
  log *String
  published **String

@aws
profile default
region eu-west-2
timeout 30
architecture arm64
