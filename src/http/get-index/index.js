const nunjucks = require('nunjucks')
nunjucks.configure('views')

function httpHeaders () {
  return {
    headers: {
      'Content-Type': 'text/html; charset=utf8',
      'Referrer-Policy': 'no-referrer',
      'X-Frame-Options': 'DENY'
    }
  }
}

exports.handler = async function http (req) {
  const body = nunjucks.render('index.njk', {})

  return {
    ...httpHeaders(),
    statusCode: 200,
    body: body
  }
}
