const logger = require('@architect/shared/logger')
const htmlResponse = require('./html')
const TikTokScraper = require('tiktok-scraper')

async function videoToMf2 (v, url) {
  return {
    type: ['h-entry'],
    properties: {
      url: [url],
      summary: [v.text],
      published: [new Date(v.createTime * 1000).toISOString()],
      author: [
        {
          type: ['h-card'],
          properties: {
            name: [v.authorMeta.nickName
            ],
            nickname: [v.authorMeta.name],
            photo: [v.authorMeta.avatar]
          }
        }
      ],
      featured: [v.imageUrl]
    }
  }
}

exports.handler = async function http (req) {
  const url = await req.queryStringParameters?.url

  if (url === undefined) {
    return await htmlResponse.render(url, null)
  }

  const container = { items: [] }
  let videoMeta
  try {
    videoMeta = await TikTokScraper.getVideoMeta(url, {})
  } catch (error) {
    logger.error({
      message: `Failed to retrieve TikTok video metadata data for ${url}`, userAgent: `${JSON.stringify(req.headers['user-agent'])}`, error: error
    })
    return {
      statusCode: 400,
      headers: {
        'cache-control': 'no-cache, no-store, must-revalidate, max-age=0, s-maxage=0',
        'content-type': 'application/mf2+json; charset=utf8'
      },
      body: JSON.stringify(container)
    }
  }

  container.items = await Promise.all(videoMeta.collector.map(v => videoToMf2(v, url)))

  logger.info({ message: `Generating Microformats2 representation for ${url}`, userAgent: `${JSON.stringify(req.headers['user-agent'])}`, container, videoMeta })

  if (await htmlResponse.isRequired(req)) {
    return await htmlResponse.render(url, container)
  }

  return {
    statusCode: 200,
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate, max-age=0, s-maxage=0',
      'content-type': 'application/mf2+json; charset=utf8'
    },
    body: JSON.stringify(container)
  }
}
