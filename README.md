# TikTok to Microformats2

A hosted service to convert metadata from TikTok videos to a [Microformats2 object](https://microformats.io).
